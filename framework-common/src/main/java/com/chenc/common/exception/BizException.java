package com.chenc.common.exception;

import com.chenc.common.error.ErrorCode;
import com.chenc.common.error.GlobalErrorCode;

/**
 * 业务异常
 *
 * @author Chen
 * @date 2022-01-26
 */
public class BizException extends AbstractException {

    private static final long serialVersionUID = 1L;

    public BizException() {
    }

    public BizException(ErrorCode errorCode) {
        this(errorCode.getCode(), errorCode.getMessage());
    }

    public BizException(String msg) {
        this(GlobalErrorCode.BIZ_ERROR.getCode(), msg);
    }

    public BizException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BizException(Throwable cause) {
        this.msg = cause == null ? null : cause.toString();
    }

}
