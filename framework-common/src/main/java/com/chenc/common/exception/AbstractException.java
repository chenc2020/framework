package com.chenc.common.exception;

/**
 * 抽象异常类
 *
 * @author Chen
 * @date 2022-01-26
 */
public class AbstractException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    /**
     * 异常码
     */
    protected Integer code;

    /**
     * 异常信息
     */
    protected String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
