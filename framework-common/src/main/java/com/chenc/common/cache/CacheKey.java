package com.chenc.common.cache;

/**
 * 缓存Key接口
 *
 * @author Chen
 * @date 2022-03-19
 */
public interface CacheKey {

    /**
     * 获取错误信息
     *
     * @return key格式
     */
    String getKeyFormat();

    /**
     * 获取错误码
     *
     * @return code
     */
    Integer getExpireTime();

}
