package com.chenc.common.api;

import com.chenc.common.error.ErrorCode;
import com.chenc.common.error.GlobalErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 统一响应结果类
 *
 * @author Chen
 * @date 2022-01-26
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 调用是否成功
     */
    private Boolean success = true;

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String message;

    /**
     * 响应结果数据
     */
    private T data;

    /**
     * traceId
     */
    private String tid;

    /**
     * 时间戳
     */
    private Long timestamp = System.currentTimeMillis();

    public Result() {
    }

    public Result(Boolean success, Integer code, String message, T data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 调用成功返回
     *
     * @param <T> 响应数据类型
     * @return 成功返回结果
     */
    public static <T> Result<T> success() {
        return success(null);
    }

    public static <T> Result<T> success(T data) {
        return new Result<T>(true, 200, "success", data);
    }

    /**
     * 调用失败返回
     *
     * @param <T> 响应数据类型
     * @return 失败返回结果
     */
    public static <T> Result<T> failed() {
        return failed(GlobalErrorCode.INTERNAL_SERVER_ERROR, null);
    }

    public static <T> Result<T> failed(ErrorCode errorCode) {
        return failed(errorCode, null);
    }

    public static <T> Result<T> failed(Integer code, String message) {
        return failed(code, message, null);
    }

    public static <T> Result<T> failed(ErrorCode errorCode, T data) {
        return failed(errorCode.getCode(), errorCode.getMessage(), data);
    }

    public static <T> Result<T> failed(Integer code, String message, T data) {
        return new Result<T>(false, code, message, data);
    }

}
