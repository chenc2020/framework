package com.chenc.common.error;

/**
 * 错误码接口
 *
 * @author Chen
 * @date 2022-01-26
 */
public interface ErrorCode {

    /**
     * 获取错误码
     *
     * @return 错误码
     */
    Integer getCode();

    /**
     * 获取错误信息
     *
     * @return 错误信息
     */
    String getMessage();

}
