package com.chenc.common.error;

/**
 * 全局通用错误码
 *
 * @author Chen
 * @date 2022-01-26
 */
public enum GlobalErrorCode implements ErrorCode {

    /**
     * 通用的业务逻辑错误
     */
    BIZ_ERROR(400, "Bad Request"),

    /**
     * 系统异常
     */
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    ;

    private Integer code;

    private String message;

    GlobalErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
