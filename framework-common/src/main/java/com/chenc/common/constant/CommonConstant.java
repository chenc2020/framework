package com.chenc.common.constant;

/**
 * 通用常量类
 *
 * @author Chen
 * @date 2022-01-27
 */
public interface CommonConstant {

    /**
     * 表示true的数值
     */
    Integer TRUE = 1;

    /**
     * 表示false的数值
     */
    Integer FALSE = 0;

}
