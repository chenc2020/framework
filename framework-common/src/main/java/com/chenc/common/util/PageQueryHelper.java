package com.chenc.common.util;

import java.util.List;

/**
 * 分页查询工具接口
 *
 * @author Chen
 * @date 2024-04-29
 */
@FunctionalInterface
public interface PageQueryHelper<E> {

    /**
     * 分页获取数据
     *
     * @param currentPage
     * @param pageSize
     * @return
     */
    List<E> pageData(long currentPage, long pageSize);

}