package com.chenc.common.util;

import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 异常工具类
 *
 * @author Chen
 * @date 2022-03-27
 */
@Slf4j
public class ThrowableUtil {

    public static String getStackTrace(Throwable throwable) {
        if (throwable == null) {
            return "";
        }
        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        } catch (Exception e) {
            log.error("get Throwable StackTrace INFO error:::" + e.getMessage(), e);
        }
        return "";
    }

}
