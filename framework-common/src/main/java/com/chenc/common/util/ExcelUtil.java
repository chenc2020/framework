package com.chenc.common.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.chenc.common.error.GlobalErrorCode;
import com.chenc.common.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Excel工具类
 *
 * @author Chen
 * @date 2024-04-29
 */
@Slf4j
public class ExcelUtil {

    private static final String SHEET1 = "Sheet1";

    private ExcelUtil() {
    }

    /**
     * 将list中的数据写出到excel文件
     *
     * @param response
     * @param fileName
     * @param dataList
     * @param clazz
     */
    public static void writeExcel(HttpServletResponse response, String fileName, List<?> dataList, Class<?> clazz) {
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name()).replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ExcelTypeEnum.XLSX.getValue());
            response.setHeader("Access-Control-Expose-Headers", "Accept-Ranges, Content-Disposition,X-Pagination-Total-Count");
            EasyExcelFactory.write(response.getOutputStream(), clazz).sheet(SHEET1).doWrite(dataList);
        } catch (Exception e) {
            log.error("导出 Excel 异常！ fileName: [{}], e: ", fileName, e);
            throw new BizException(GlobalErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 分页写出到excel文件
     *
     * @param response
     * @param fileName
     * @param clazz
     * @param pageQueryHelper
     * @param totalCount
     * @param pageSize
     */
    public static void writeExcelPage(HttpServletResponse response, String fileName, Class<?> clazz, PageQueryHelper pageQueryHelper,
                                      long totalCount, long pageSize) {
        ExcelWriter excelWriter = null;
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name()).replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ExcelTypeEnum.XLSX.getValue());
            response.setHeader("Access-Control-Expose-Headers", "Accept-Ranges, Content-Disposition,X-Pagination-Total-Count");
            excelWriter = EasyExcel.write(response.getOutputStream(), clazz).build();
            WriteSheet writeSheet = EasyExcel.writerSheet(SHEET1).build();
            long writeCompletedCount = 0L;
            long maxPageNum = (totalCount - 1L) / pageSize + 1L;
            long currentPage = 1L;
            while (currentPage <= maxPageNum) {
                List data = pageQueryHelper.pageData(currentPage, pageSize);
                if (CollectionUtils.isEmpty(data)) {
                    excelWriter.write(data, writeSheet);
                    break;
                }
                if (data.size() < pageSize) {
                    excelWriter.write(data, writeSheet);
                    break;
                }
                writeCompletedCount += data.size();
                if (writeCompletedCount > totalCount) {
                    data = data.subList(0, (int) (writeCompletedCount - totalCount) + 1);
                    excelWriter.write(data, writeSheet);
                    break;
                }
                if (writeCompletedCount == totalCount) {
                    excelWriter.write(data, writeSheet);
                    break;
                }
                excelWriter.write(data, writeSheet);
                ++currentPage;
            }
        } catch (Exception e) {
            log.error("分页导出 Excel 异常！ fileName: [{}], e: ", fileName, e);
            throw new BizException(GlobalErrorCode.INTERNAL_SERVER_ERROR);
        } finally {
            if (excelWriter != null) {
                excelWriter.finish();
            }
        }
    }

}
