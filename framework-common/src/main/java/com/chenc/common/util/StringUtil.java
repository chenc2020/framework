package com.chenc.common.util;

import java.util.UUID;

/**
 * 字符串工具类
 *
 * @author Chen
 * @date 2022-03-19
 */
public class StringUtil {

    /**
     * 获取 UUID 字符串
     *
     * @return 去除 “-” 后的UUID 字符串
     */
    public static String getUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
