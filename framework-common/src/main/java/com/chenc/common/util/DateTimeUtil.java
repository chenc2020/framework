package com.chenc.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 日期时间工具类
 *
 * @author Chen
 * @date 2022-03-19
 */
public class DateTimeUtil {

    /**
     * 日期格式
     * 年-月-日 时:分:秒
     * eg：2022-03-19 15:00:05
     */
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final String YYYY_MM = "yyyy-MM";

    public static final String YYYY = "yyyy";

    public static final String START_TIME = "00:00:00";

    public static final String END_TIME = "23:59:59";

    public static final String SPACE = " ";

    // -------------------- format date ------------------------

    public static String format(Date date) {
        return format(date, YYYY_MM_DD_HH_MM_SS);
    }

    public static String format(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    public static String format(Date date, String pattern, TimeZone timeZone) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        sdf.setTimeZone(timeZone);
        return sdf.format(date);
    }

    // -------------------- parse date ------------------------

    public static Date parse(String dateStr) throws ParseException {
        return parse(dateStr, YYYY_MM_DD_HH_MM_SS);
    }

    public static Date parse(String dateStr, String pattern) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.parse(dateStr);
    }

    public static Date parse(String dateStr, String pattern, TimeZone timeZone) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        if (timeZone != null) {
            sdf.setTimeZone(timeZone);
        }
        return sdf.parse(dateStr);
    }

    // --------------------  Calendar  ------------------------

    public static Calendar toCalendar(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static Date toDate(Calendar calendar) {
        return calendar.getTime();
    }

    // --------------------  mills  ------------------------

    public static Date toDate(long mills) {
        return new Date(mills);
    }

    public static long getLeftMills() {
        Calendar c = Calendar.getInstance();
        long timeInMillis = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTimeInMillis() - timeInMillis;
    }

    // --------------------  LocalDateTime ------------------------

    public static LocalDateTime toLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime toLocalDateTime(String dateStr) {
        return toLocalDateTime(dateStr, YYYY_MM_DD_HH_MM_SS);
    }

    public static LocalDateTime toLocalDateTime(String dateStr, String pattern) {
        return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(pattern));
    }

    public static String format(LocalDateTime localDateTime) {
        return format(localDateTime, YYYY_MM_DD_HH_MM_SS);
    }

    public static String format(LocalDateTime localDateTime, String pattern) {
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    // --------------------  appoint ------------------------

    /**
     * 获取指定日期开始时间（0点）
     *
     * @param date
     * @return
     */
    public static Date getStart(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * 获取指定日期结束时间
     *
     * @param date
     * @return
     */
    public static Date getEnd(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    public static Date addYear(Date date, int add) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, add);
        return c.getTime();
    }

    public static Date addMonth(Date date, int add) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, add);
        return c.getTime();
    }

    public static Date addDay(Date date, int add) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_MONTH, add);
        return c.getTime();
    }

    public static Date addHour(Date date, int add) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR_OF_DAY, add);
        return c.getTime();
    }

    public static Date addMinute(Date date, int add) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, add);
        return c.getTime();
    }

    public static Date addSecond(Date date, int add) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.SECOND, add);
        return c.getTime();
    }

}
