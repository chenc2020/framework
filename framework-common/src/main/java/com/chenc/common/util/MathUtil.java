package com.chenc.common.util;

/**
 * 数学运算工具类
 *
 * @author Chen
 * @date 2022-03-19
 */
public class MathUtil {

    /**
     * 返回保留两位小数后的字符串(四舍五入)
     *
     * @param d 数值
     * @return
     */
    public static String retainTwoPlaces(double d) {
        return String.format("%.2f", d);
    }

}
