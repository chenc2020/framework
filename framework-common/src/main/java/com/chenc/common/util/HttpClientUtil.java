package com.chenc.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Http Client 工具类
 *
 * @author Chen
 * @date 2022-03-19
 */
@Slf4j
public class HttpClientUtil {

    /**
     * 设置连接超时时间，单位毫秒。
     */
    private static final int CONNECT_TIMEOUT = 15000;

    /**
     * 请求获取数据的超时时间(即响应时间)，单位毫秒。
     */
    private static final int SOCKET_TIMEOUT = 60000;

    /**
     * 编码格式
     */
    private static final String ENCODING = "UTF-8";

    private static RequestConfig getRequestConfig() {
        return RequestConfig.custom().setConnectTimeout(CONNECT_TIMEOUT).setSocketTimeout(SOCKET_TIMEOUT).build();
    }

    public static String get(String url) {
        return get(url, null, null);
    }

    /**
     * 发送 GET 方式请求
     *
     * @param url
     * @param headers
     * @param params
     * @return
     */
    public static String get(String url, Map<String, String> headers, Map<String, String> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse httpResponse = null;
        try {
            URIBuilder uriBuilder = new URIBuilder(url);
            if (params != null && !params.isEmpty()) {
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    uriBuilder.addParameter(entry.getKey(), entry.getValue());
                }
            }
            HttpGet httpGet = new HttpGet(uriBuilder.build());
            httpGet.setConfig(getRequestConfig());
            setHeaders(httpGet, headers);
            httpResponse = httpClient.execute(httpGet);
            return getResult(httpResponse);
        } catch (Exception e) {
            log.error("HTTP GET 请求异常信息记录: ", e);
        } finally {
            release(httpResponse, httpClient);
        }
        return null;
    }

    /**
     * 发送 POST JSON方式请求
     *
     * @param url
     * @param headers
     * @param params
     * @return
     */
    public static String postJson(String url, Map<String, String> headers, Map<String, String> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse httpResponse = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(getRequestConfig());
            setHeaders(httpPost, headers);
            setJsonParams(httpPost, params);
            httpResponse = httpClient.execute(httpPost);
            return getResult(httpResponse);
        } catch (Exception e) {
            log.error("HTTP POST JSON 请求异常信息记录: ", e.getMessage());
        } finally {
            release(httpResponse, httpClient);
        }
        return null;
    }

    /**
     * 发送 POST 表单方式请求
     *
     * @param url
     * @param headers
     * @param params
     * @return
     */
    public static String postForm(String url, Map<String, String> headers, Map<String, String> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse httpResponse = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(getRequestConfig());
            setHeaders(httpPost, headers);
            setFormParams(httpPost, params);
            httpResponse = httpClient.execute(httpPost);
            return getResult(httpResponse);
        } catch (Exception e) {
            log.error("HTTP POST FORM 请求异常信息记录: ", e);
        } finally {
            release(httpResponse, httpClient);
        }
        return null;
    }

    /**
     * 发送 PUT 方式请求
     *
     * @param url
     * @param headers
     * @param params
     * @return
     */
    public static String put(String url, Map<String, String> headers, Map<String, String> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse httpResponse = null;
        try {
            HttpPut httpPut = new HttpPut(url);
            httpPut.setConfig(getRequestConfig());
            setHeaders(httpPut, headers);
            setJsonParams(httpPut, params);
            httpResponse = httpClient.execute(httpPut);
            return getResult(httpResponse);
        } catch (Exception e) {
            log.error("HTTP PUT 请求异常信息记录: {}", e);
        } finally {
            release(httpResponse, httpClient);
        }
        return null;
    }


    /**
     * 发送 DELETE 方式请求
     *
     * @param url
     * @return
     */
    public static String delete(String url, Map<String, String> headers) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse httpResponse = null;
        try {
            HttpDelete httpDelete = new HttpDelete(url);
            httpDelete.setConfig(getRequestConfig());
            setHeaders(httpDelete, headers);
            httpResponse = httpClient.execute(httpDelete);
            return getResult(httpResponse);
        } catch (Exception e) {
            log.error("HTTP DELETE 请求异常信息记录: ", e);
        } finally {
            release(httpResponse, httpClient);
        }
        return null;
    }

    /**
     * 发送 PATCH 方式请求
     *
     * @param url
     * @param headers
     * @param params
     * @return
     */
    public static String patch(String url, Map<String, String> headers, Map<String, String> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse httpResponse = null;
        try {
            HttpPatch httpPatch = new HttpPatch(url);
            httpPatch.setConfig(getRequestConfig());
            setHeaders(httpPatch, headers);
            setJsonParams(httpPatch, params);
            httpResponse = httpClient.execute(httpPatch);
            return getResult(httpResponse);
        } catch (Exception e) {
            log.error("HTTP PATCH 请求异常信息记录: ", e);
        } finally {
            release(httpResponse, httpClient);
        }
        return null;
    }

    /**
     * 获取响应结果
     *
     * @param httpResponse
     * @return
     * @throws IOException
     */
    private static String getResult(CloseableHttpResponse httpResponse) throws IOException {
        if (httpResponse != null && httpResponse.getStatusLine() != null) {
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                return EntityUtils.toString(httpEntity, Charset.forName(ENCODING));
            }
        }
        return null;
    }

    /**
     * 设置请求头参数
     *
     * @param httpMethod
     * @param headers
     */
    private static void setHeaders(HttpRequestBase httpMethod, Map<String, String> headers) {
        if (headers != null && !headers.isEmpty()) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpMethod.setHeader(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * 设置 Json 请求参数
     *
     * @param httpMethod
     * @param params
     */
    private static void setJsonParams(HttpEntityEnclosingRequestBase httpMethod, Map<String, String> params) {
        if (params != null && !params.isEmpty()) {
            StringEntity stringEntity = new StringEntity(params.toString(), Charset.forName(ENCODING));
            stringEntity.setContentType(ContentType.APPLICATION_JSON.getMimeType());
            httpMethod.setEntity(stringEntity);
        }
    }

    /**
     * 设置表单请求参数
     *
     * @param httpMethod
     * @param params
     */
    private static void setFormParams(HttpEntityEnclosingRequestBase httpMethod, Map<String, String> params) {
        if (params != null && !params.isEmpty()) {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            Set<Map.Entry<String, String>> entrySet = params.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                nameValuePairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            httpMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs, Charset.forName(ENCODING)));
        }
    }

    /**
     * 释放资源
     *
     * @param httpResponse
     * @param httpClient
     */
    private static void release(CloseableHttpResponse httpResponse, CloseableHttpClient httpClient) {
        if (httpResponse != null) {
            try {
                httpResponse.close();
            } catch (IOException e) {
                log.error("httpResponse close fail!", e);
            }
        }
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (IOException e) {
                log.error("httpClient close fail!", e);
            }
        }
    }

}
