package com.chenc.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Chen
 * @date 2024-04-29
 */
public class ObjectUtil {

    public static int getOrDefault(Integer o) {
        return Objects.isNull(o) ? 0 : o;
    }

    public static long getOrDefault(Long o) {
        return Objects.isNull(o) ? 0L : o;
    }

    public static double getOrDefault(Double o) {
        return Objects.isNull(o) ? 0.0 : o;
    }

    public static float getOrDefault(Float o) {
        return Objects.isNull(o) ? 0 : o;
    }

    public static short getOrDefault(Short o) {
        return Objects.isNull(o) ? 0 : o;
    }

    public static byte getOrDefault(Byte o) {
        return Objects.isNull(o) ? 0 : o;
    }

    public static boolean getOrDefault(Boolean o) {
        return Objects.nonNull(o) && o;
    }

    public static String getOrDefault(String o) {
        return Objects.isNull(o) ? "" : o;
    }

    public static Object[] getOrDefault(Object[] o) {
        return Objects.isNull(o) ? new Object[0] : o;
    }

    public static <T> List<T> getOrDefault(List<T> o) {
        return Objects.isNull(o) ? new ArrayList<>() : o;
    }

}
