package com.chenc.common.base;

import java.io.Serializable;

/**
 * DTO 基类
 *
 * @author Chen
 * @date 2022-01-27
 */
public class BaseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

}
