package com.chenc.common.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 分页数据封装类
 *
 * @author Chen
 * @date 2022-01-26
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@EqualsAndHashCode
public class PageInfo<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 默认当前页码
     */
    private static final Long DEFAULT_CURRENT_PAGE = 1L;

    /**
     * 默认当前页记录数
     */
    private static final Long DEFAULT_PAGE_SIZE = 10L;


    /**
     * 当前页
     */
    private Long currentPage;

    /**
     * 每页记录数
     */
    private Long pageSize;

    /**
     * 总页数
     */
    private Long totalPage;

    /**
     * 总记录数
     */
    private Long totalCount;

    /**
     * 分页数据集合
     */
    private List<T> records;

    public PageInfo() {
        this.currentPage = DEFAULT_CURRENT_PAGE;
        this.pageSize = DEFAULT_PAGE_SIZE;
    }

    public PageInfo(List<T> records) {
        this();
        this.records = records;
    }

    public PageInfo(Long currentPage, Long pageSize, Long totalPage, Long totalCount, List<T> records) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
        this.totalCount = totalCount;
        this.records = records;
    }

}
