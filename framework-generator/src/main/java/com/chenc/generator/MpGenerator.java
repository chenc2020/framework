package com.chenc.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * Mybatis Plus 代码自动生成器
 *
 * @author Chen
 * @date 2020-11-19
 */
public class MpGenerator {

    /**
     * 作者名称
     */
    private static String authorName = "Chen";
    /**
     * 数据库地址
     */
    private static String dbUrl = "jdbc:mysql://localhost:3307/my_testdb?characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT%2B8";
    /**
     * 数据库用户名
     */
    private static String dbUsername = "root";
    /**
     * 数据库密码
     */
    private static String dbPassword = "root";
    /**
     * 父包
     */
    private static String parentPackage = "com.chenc.frameworkexample";
    /**
     * 表前缀
     */
    private static String tablePrefix = "t_";
    /**
     * 表名称数组
     */
    private static String[] tableNames = {"t_user"};

    private static String modulePath = "/framework-generator/";

    public static void main(String[] args) {
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + modulePath + "/src/main/java")
                .setAuthor(authorName)
                .setOpen(false)
                .setFileOverride(true)
                .setIdType(IdType.AUTO)
                .setEntityName("%s")
                .setMapperName("%sDAO")
                .setServiceName("%sManager")
                .setServiceImplName("%sManagerImpl")
                .setBaseResultMap(true)
                .setBaseColumnList(true);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setDriverName("com.mysql.cj.jdbc.Driver")
                .setUsername(dbUsername)
                .setPassword(dbPassword);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(parentPackage)
                //.setModuleName("dashboard-core")
                .setController("controller")
                .setService("manager")
                .setServiceImpl("manager.impl")
                .setMapper("dao")
                .setEntity("entity")
                .setXml("mapper");

        // 策略配置
        StrategyConfig st = new StrategyConfig();
        st.setCapitalMode(true)
                .setNaming(NamingStrategy.underline_to_camel)
                //*** 设置表前缀
                .setTablePrefix(tablePrefix)
                //*** 设置表名
                .setInclude(tableNames)
                .setEntityLombokModel(true);

        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        mpg.setGlobalConfig(gc)
                .setDataSource(dsc)
                .setPackageInfo(pc)
                .setStrategy(st);

        mpg.execute();
    }

}
