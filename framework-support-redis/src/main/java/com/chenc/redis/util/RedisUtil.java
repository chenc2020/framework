package com.chenc.redis.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis工具类
 *
 * @author Chen
 * @date 2022-03-19
 */
public class RedisUtil {

    private final static Logger log = LoggerFactory.getLogger(RedisUtil.class);

    private RedisTemplate<String, Object> redisTemplate;

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // ------------------------ common --------------------------------

    /**
     * 设置缓存过期时间
     *
     * @param key  键
     * @param time 过期时间（单位：秒）
     * @return
     */
    public boolean expire(String key, long time) {
        redisTemplate.expire(key, time, TimeUnit.SECONDS);
        return true;
    }

    /**
     * 获取缓存过期时间
     *
     * @param key 键
     * @return 过期时间（单位：秒），返回0代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断指定key是否存在
     *
     * @param key 键
     * @return
     */
    public boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 删除缓存
     *
     * @param keys 一个或多个键
     */
    public void del(String... keys) {
        if (keys != null && keys.length > 0) {
            if (keys.length == 1) {
                redisTemplate.delete(keys[0]);
            } else {
                redisTemplate.delete(List.of(keys));
            }
        }
    }

    // ------------------------ string --------------------------------

    /**
     * 获取缓存
     *
     * @param key
     * @return
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    public boolean set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
        return true;
    }

    public boolean set(String key, Object value, long expire) {
        redisTemplate.opsForValue().set(key, value, expire, TimeUnit.SECONDS);
        return true;
    }

    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    // ------------------------ hash --------------------------------

    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }

    public boolean hset(String key, String item, Object value) {
        redisTemplate.opsForHash().put(key, item, value);
        return true;
    }

    public boolean hdel(String key, Object... items) {
        redisTemplate.opsForHash().delete(key, items);
        return true;
    }

    public boolean hHashKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    public double hIncr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    public double hDecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    public Object hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    public boolean hmset(String key, Map<String, Object> map) {
        redisTemplate.opsForHash().putAll(key, map);
        return true;
    }

    public boolean hmset(String key, Map<String, Object> map, long expire) {
        redisTemplate.opsForHash().putAll(key, map);
        if (expire > 0) {
            expire(key, expire);
        }
        return true;
    }

    // ------------------------ list --------------------------------

    public List<Object> lGet(String key, long start, long end) {
        return redisTemplate.opsForList().range(key, start, end);
    }

    public long lSize(String key) {
        return redisTemplate.opsForList().size(key);
    }

    public Object lGetByIndex(String key, long index) {
        return redisTemplate.opsForList().index(key, index);
    }

    public boolean lRight(String key, Object value) {
        redisTemplate.opsForList().rightPush(key, value);
        return true;
    }

    public boolean lLeft(String key, Object value) {
        redisTemplate.opsForList().leftPush(key, value);
        return true;
    }


    public boolean lRightAndExpire(String key, Object value, long time) {
        redisTemplate.opsForList().rightPush(key, value);
        if (time > 0) {
            expire(key, time);
        }
        return true;
    }

    public boolean lLeftAndExpire(String key, Object value, long time) {
        redisTemplate.opsForList().leftPush(key, value);
        if (time > 0) {
            expire(key, time);
        }
        return true;
    }

    public boolean lRight(String key, List<Object> values) {
        redisTemplate.opsForList().rightPushAll(key, values);
        return true;
    }

    public boolean lLeft(String key, List<Object> values) {
        redisTemplate.opsForList().leftPushAll(key, values);
        return true;
    }


    public boolean lRightAndExpire(String key, List<Object> values, long time) {
        redisTemplate.opsForList().rightPushAll(key, values);
        if (time > 0) {
            expire(key, time);
        }
        return true;
    }

    public boolean lLeftAndExpire(String key, List<Object> values, long time) {
        redisTemplate.opsForList().leftPushAll(key, values);
        if (time > 0) {
            expire(key, time);
        }
        return true;
    }

    public boolean lUpdateIndex(String key, long index, Object value) {
        redisTemplate.opsForList().set(key, index, value);
        return true;
    }

    public long lRemove(String key, long count, Object value) {
        return redisTemplate.opsForList().remove(key, count, value);
    }

    // ------------------------ set --------------------------------

    public Set<Object> sGet(String key) {
        return redisTemplate.opsForSet().members(key);
    }

    public long sSet(String key, Object... values) {
        return redisTemplate.opsForSet().add(key, values);
    }

    public boolean sHasKey(String key, Object value) {
        return redisTemplate.opsForSet().isMember(key, value);
    }

    public long sSetAndExpire(String key, long time, Object... values) {
        long count = redisTemplate.opsForSet().add(key, values);
        if (time > 0) {
            expire(key, time);
        }
        return count;
    }

    public long sSize(String key) {
        return redisTemplate.opsForSet().size(key);
    }

    public long sRemove(String key, Object... values) {
        return redisTemplate.opsForSet().remove(key, values);
    }


}
