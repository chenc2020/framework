package com.chenc.common.util;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author Chen
 * @date 2024-04-29
 */
public class ObjectUtilTest {

    @Test
    public void testGetOrDefault() {
        System.out.println(ObjectUtil.getOrDefault(1));
        System.out.println(ObjectUtil.getOrDefault(2));
        System.out.println(ObjectUtil.getOrDefault((byte) 3));
        System.out.println(ObjectUtil.getOrDefault((short) 4));
        System.out.println(ObjectUtil.getOrDefault(5.1));
        System.out.println(ObjectUtil.getOrDefault((float) 6.2));
        System.out.println(ObjectUtil.getOrDefault(""));
        System.out.println(Arrays.toString(ObjectUtil.getOrDefault(new Object[2])));
        System.out.println(ObjectUtil.getOrDefault(Collections.emptyList()));
    }

}
