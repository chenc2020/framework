package com.chenc.common.util;


import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Chen
 * @date 2024-04-29
 */
public class DateTimeUtilTest {

    @Test
    public void testDateTime() {
        System.out.println("--- specify ---");
        Date date = new Date();
        System.out.println(DateTimeUtil.getStart(date));
        System.out.println(DateTimeUtil.getEnd(date));

        System.out.println("--- localDateTime ---");
        System.out.println(DateTimeUtil.toLocalDateTime(new Date()));
        System.out.println(DateTimeUtil.toDate(LocalDateTime.now()));
        System.out.println(DateTimeUtil.toLocalDateTime(DateTimeUtil.format(date)));
        System.out.println(DateTimeUtil.toLocalDateTime(DateTimeUtil.format(date, DateTimeUtil.YYYY_MM_DD_HH_MM_SS), DateTimeUtil.YYYY_MM_DD_HH_MM_SS));
        System.out.println(DateTimeUtil.format(LocalDateTime.now()));
        System.out.println(DateTimeUtil.format(LocalDateTime.now(), DateTimeUtil.YYYY_MM_DD_HH_MM_SS));

        System.out.println("--- mills ---");
        System.out.println(DateTimeUtil.toDate(System.currentTimeMillis()));
        System.out.println(DateTimeUtil.getLeftMills() + "毫秒");
        System.out.println(DateTimeUtil.getLeftMills() / 60000 + "分钟");

        System.out.println("--- appoint ---");
        System.out.println(date);
        System.out.println(DateTimeUtil.addYear(date, 10));
        System.out.println(DateTimeUtil.addMonth(date, 10));
        System.out.println(DateTimeUtil.addDay(date, 100));
        System.out.println(DateTimeUtil.addHour(date, 15));
        System.out.println(DateTimeUtil.addMinute(date, 120));
        System.out.println(DateTimeUtil.addSecond(date, 66));
    }

}
