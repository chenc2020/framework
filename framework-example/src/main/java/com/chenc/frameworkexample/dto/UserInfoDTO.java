package com.chenc.frameworkexample.dto;

import lombok.Data;

/**
 * @author Chen
 * @date 2024-04-29
 */
@Data
public class UserInfoDTO {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户名称
     */
    private String username;

}
