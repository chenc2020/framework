package com.chenc.frameworkexample.dto.resp;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 用一句话描述这个文件的作用
 *
 * @author Chen
 * @date 2022-03-19
 */
@Data
public class UserGetResp {

    private Long id;

    private String name;

    private Integer age;

    private Integer sex;

    private String sexDesc;

    private LocalDateTime createTime;

}
