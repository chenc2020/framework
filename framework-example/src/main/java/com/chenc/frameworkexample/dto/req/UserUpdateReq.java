package com.chenc.frameworkexample.dto.req;

import lombok.Data;

/**
 * 用一句话描述这个文件的作用
 *
 * @author Chen
 * @date 2022-03-19
 */
@Data
public class UserUpdateReq {

    /**
     * 用户id
     */
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别（0：未知；1：男；2：女；）
     */
    private Integer sex;

}
