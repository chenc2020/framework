package com.chenc.frameworkexample.aop;

import com.alibaba.fastjson.JSON;
import com.chenc.common.api.Result;
import com.chenc.common.util.StringUtil;
import com.chenc.frameworkexample.util.ThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Objects;

/**
 * @author Chen
 * @date 2024-04-29
 */

@Slf4j
@Aspect
@Component
public class RequestAop {

    private int slowResponseTime = 1000;

    @Pointcut("execution(* com.chenc.frameworkexample.controller.*.*(..))")
    public void requestHandler() {
        //this is an blank body
    }

    @Around("requestHandler()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(
                RequestContextHolder.getRequestAttributes())).getRequest();
        String requestURI = request.getRequestURI();
        String method = request.getMethod();
        Signature signature = pjp.getSignature();
        Object[] args = pjp.getArgs();
        ThreadLocalUtil.setTid(StringUtil.getUuid());
        log.info("userInfo: {},  requestURI: {}, args: {}", ThreadLocalUtil.getUserInfo(), requestURI, Arrays.toString(args));
        Object result = pjp.proceed();
        if (result instanceof Result) {
            Result resultData = (Result) result;
            resultData.setTid(ThreadLocalUtil.getTid());
        }
        long endTime = System.currentTimeMillis();
        long spendTime = endTime - startTime;
        if (spendTime < slowResponseTime) {
            log.info("spendTime: {}ms, method：{} requestURI: {}, signature：{}, args: {}, result: {}",
                    spendTime,  method, requestURI, signature, Arrays.toString(args), JSON.toJSONString(result));
        } else {
            log.warn("spendTime: {}ms, method：{} requestURI: {}, signature：{}, args: {}, result: {}",
                    spendTime,  method, requestURI, signature, Arrays.toString(args), JSON.toJSONString(result));
        }
        return result;
    }

}
