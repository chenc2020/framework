package com.chenc.frameworkexample.util;

import com.chenc.frameworkexample.dto.UserInfoDTO;

/**
 * @author Chen
 * @date 2024-04-29
 */
public class ThreadLocalUtil {

    private static ThreadLocal<UserInfoDTO> userInfoThreadLocal = new ThreadLocal<>();

    private static ThreadLocal<String> tidThreadLocal = new ThreadLocal<>();

    private static ThreadLocal<String> ipThreadLocal = new ThreadLocal<>();

    public static void setTid(String tid) {
        tidThreadLocal.set(tid);
    }

    public static String getTid() {
        return tidThreadLocal.get();
    }

    public static UserInfoDTO getUserInfo() {
        return userInfoThreadLocal.get();
    }

    public static void setUserInfo(UserInfoDTO dto) {
        userInfoThreadLocal.set(dto);
    }

    public static String getIp() {
        String ip = ipThreadLocal.get();
        return ip == null ? "0.0.0.0" : ip;
    }

    public static void setIp(String ip) {
        ipThreadLocal.set(ip);
    }

}
