package com.chenc.frameworkexample.controller;


import com.chenc.common.api.Result;
import com.chenc.frameworkexample.dto.req.UserAddReq;
import com.chenc.frameworkexample.dto.req.UserUpdateReq;
import com.chenc.frameworkexample.dto.resp.UserGetResp;
import com.chenc.frameworkexample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author Chen
 * @since 2022-03-19
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public Result<UserGetResp> getUser(@RequestParam Long id) {
        return Result.success(userService.getUser(id));
    }

    @PostMapping
    public Result<Boolean> addUser(@RequestBody UserAddReq userAddReq) {
        return Result.success(userService.addUser(userAddReq));
    }

    @PutMapping
    public Result<Boolean> updateUser(@RequestBody UserUpdateReq userUpdateReq) {
        return Result.success(userService.updateUser(userUpdateReq));
    }

    @DeleteMapping
    public Result<Boolean> deleteUser(@RequestParam Long id) {
        return Result.success(userService.deleteUser(id));
    }

}

