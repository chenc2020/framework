package com.chenc.frameworkexample.controller;

import com.chenc.common.api.Result;
import com.chenc.common.exception.BizException;
import com.chenc.common.util.IpUtil;
import com.chenc.frameworkexample.component.RedisService;
import com.chenc.frameworkexample.enums.CacheEnum;
import com.chenc.frameworkexample.enums.ErrorCodeEnum;
import com.chenc.redis.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * 测试控制器
 *
 * @author Chen
 * @date 2022-01-27
 */
@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private RedisService redisService;
    @Autowired
    private RedisUtil redisUtil;

    @GetMapping("/time")
    public String getTime(HttpServletRequest request) {
        String ip = IpUtil.getIpAddr(request);
        LocalDateTime now = LocalDateTime.now();
        log.info("请求ip: {}, 获取当前时间：{}", ip, now);
        return "Hello! current time: " + now;
    }

    @GetMapping("/exception-handler")
    public Result<Void> testExceptionHandler(@RequestParam Integer type) {
        if (type == null) {
            int i = 1 / 0;
        } else if (type == 1) {
            throw new BizException(ErrorCodeEnum.PARAM_ERROR);
        }
        return Result.success();
    }

    @GetMapping("/redis")
    public Result<Object> getRedis(@RequestParam String key) {
        Object value = redisUtil.get(key);
        return Result.success(value);
    }

    @GetMapping("/setKey")
    public Result<Object> setKey(@RequestParam String key,
                         @RequestParam String value) {
        // 获取value
        Object redisValue = redisService.get(CacheEnum.TEST_KEY);
        if (redisValue != null) {
            return Result.success(redisValue);
        }
        log.error("set key value to redis");
        redisService.set(CacheEnum.TEST_KEY, value, key);
        return Result.success(redisValue);
    }

}
