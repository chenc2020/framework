package com.chenc.frameworkexample.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chenc.frameworkexample.dao.UserDAO;
import com.chenc.frameworkexample.entity.User;
import com.chenc.frameworkexample.manager.UserManager;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author Chen
 * @since 2022-03-19
 */
@Service
public class UserManagerImpl extends ServiceImpl<UserDAO, User> implements UserManager {

    @Override
    public User getUser(Long id) {
        LambdaQueryWrapper<User> qw = new LambdaQueryWrapper<>();
        qw.eq(User::getId, id);
        qw.eq(User::getIsDelete, 0);
        return getOne(qw);
    }

    @Override
    public boolean deleteUser(Long id) {
        LambdaUpdateWrapper<User> uw = new LambdaUpdateWrapper<>();
        uw.eq(User::getId, id);
        uw.set(User::getIsDelete, 1);
        return update(uw);
    }

}
