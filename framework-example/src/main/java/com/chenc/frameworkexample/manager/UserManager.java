package com.chenc.frameworkexample.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenc.frameworkexample.entity.User;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author Chen
 * @since 2022-03-19
 */
public interface UserManager extends IService<User> {

    /**
     * 获取用户信息
     *
     * @param id 用户id
     * @return 用户信息
     */
    User getUser(Long id);

    /**
     * 删除用户
     *
     * @param id 用户id
     * @return 结果
     */
    boolean deleteUser(Long id);

}
