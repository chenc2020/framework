package com.chenc.frameworkexample.component;

import com.chenc.frameworkexample.enums.CacheEnum;
import com.chenc.redis.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Redis 扩展服务
 *
 * @author Chen
 * @date 2022-03-19
 */
@Slf4j
@Service
public class RedisService {

    @Autowired
    private RedisUtil redisUtil;

    public boolean set(CacheEnum cacheEnum, Object value, Object... keys) {
        String redisKey = getRedisKey(cacheEnum, keys);
        return redisUtil.set(redisKey, value, cacheEnum.getExpireTime());
    }

    public Object get(CacheEnum cacheEnum, Object... keys) {
        String redisKey = getRedisKey(cacheEnum, keys);
        return redisUtil.get(redisKey);
    }

    public String getRedisKey(CacheEnum cacheEnum, Object... keys) {
        return String.format(cacheEnum.getKeyFormat(), keys);
    }

}
