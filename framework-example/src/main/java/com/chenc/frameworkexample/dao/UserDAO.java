package com.chenc.frameworkexample.dao;

import com.chenc.frameworkexample.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Chen
 * @since 2022-03-19
 */
public interface UserDAO extends BaseMapper<User> {

}
