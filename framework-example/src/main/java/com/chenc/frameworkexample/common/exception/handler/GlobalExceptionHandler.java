package com.chenc.frameworkexample.common.exception.handler;

import com.chenc.common.api.Result;
import com.chenc.common.exception.BizException;
import com.chenc.common.util.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局统一异常处理类
 *
 * @author Chen
 * @date 2022-03-27
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理所有未知异常
     *
     * @param t
     * @return
     */
    @ExceptionHandler(Throwable.class)
    public Result defaultExceptionHandler(Throwable t) {
        // 打印堆栈信息
        log.error("defaultExceptionHandler: {}", ThrowableUtil.getStackTrace(t));
        return Result.failed();
    }

    /**
     * 处理业务异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(BizException.class)
    public Result bizExceptionHandler(BizException e) {
        // 打印堆栈信息
        log.error("bizExceptionHandler: {}", ThrowableUtil.getStackTrace(e));
        return Result.failed(e.getCode(), e.getMsg());
    }

}
