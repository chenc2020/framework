package com.chenc.frameworkexample.enums;

import com.chenc.common.cache.CacheKey;

/**
 * 缓存枚举
 *
 * @author Chen
 * @date 2022-03-19
 */
public enum CacheEnum implements CacheKey {

    /**
     * 用户缓存
     */
    USER_KEY("user:%s", 60),

    TEST_KEY("test:%s", 60),

    ;

    /**
     * 缓存key格式
     */
    private String keyFormat;

    /**
     * 缓存过期时间
     */
    private Integer expireTime;

    CacheEnum(String keyFormat, Integer expireTime) {
        this.keyFormat = keyFormat;
        this.expireTime = expireTime;
    }

    @Override
    public String getKeyFormat() {
        return this.keyFormat;
    }

    @Override
    public Integer getExpireTime() {
        return this.expireTime;
    }

}
