package com.chenc.frameworkexample.enums;

import com.chenc.common.error.ErrorCode;

/**
 * 错误枚举
 *
 * @author Chen
 * @date 2022-03-27
 */
public enum ErrorCodeEnum implements ErrorCode {

    /**
     * 参数异常
     */
    PARAM_ERROR(1001, "Param Error!"),
    ;

    private Integer code;

    private String message;

    ErrorCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
