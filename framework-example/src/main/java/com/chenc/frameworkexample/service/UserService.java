package com.chenc.frameworkexample.service;

import com.chenc.frameworkexample.dto.req.UserAddReq;
import com.chenc.frameworkexample.dto.req.UserUpdateReq;
import com.chenc.frameworkexample.dto.resp.UserGetResp;

/**
 * 用一句话描述这个文件的作用
 *
 * @author Chen
 * @date 2022-03-19
 */
public interface UserService {

    /**
     * 根据id获取用户信息
     *
     * @param id 用户id
     * @return 用户信息
     */
    UserGetResp getUser(Long id);

    /**
     * 新增用户
     *
     * @param userAddReq
     * @return 结果
     */
    boolean addUser(UserAddReq userAddReq);

    /**
     * 更新用户
     *
     * @param userUpdateReq
     * @return 结果
     */
    boolean updateUser(UserUpdateReq userUpdateReq);

    /**
     * 根据id删除用户信息
     *
     * @param id 用户id
     * @return 结果
     */
    boolean deleteUser(Long id);

}
