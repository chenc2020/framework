package com.chenc.frameworkexample.service.impl;

import com.chenc.frameworkexample.dto.req.UserAddReq;
import com.chenc.frameworkexample.dto.req.UserUpdateReq;
import com.chenc.frameworkexample.dto.resp.UserGetResp;
import com.chenc.frameworkexample.entity.User;
import com.chenc.frameworkexample.manager.UserManager;
import com.chenc.frameworkexample.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用一句话描述这个文件的作用
 *
 * @author Chen
 * @date 2022-03-19
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserManager userManager;

    @Override
    public UserGetResp getUser(Long id) {
        User user = userManager.getUser(id);
        UserGetResp userGetResp = new UserGetResp();
        userGetResp.setId(user.getId());
        userGetResp.setName(user.getName());
        userGetResp.setAge(user.getAge());
        Integer sex = user.getSex();
        String sexDesc;
        if (sex == 1) {
            sexDesc = "男";
        } else if (sex == 2) {
            sexDesc = "女";
        } else {
            sexDesc = "未知";
        }
        userGetResp.setSex(sex);
        userGetResp.setSexDesc(sexDesc);
        userGetResp.setCreateTime(user.getCreateTime());
        return userGetResp;
    }

    @Override
    public boolean addUser(UserAddReq userAddReq) {
        User addUser = new User();
        addUser.setName(userAddReq.getName());
        addUser.setAge(userAddReq.getAge());
        addUser.setSex(userAddReq.getSex());
        return userManager.save(addUser);
    }

    @Override
    public boolean updateUser(UserUpdateReq userUpdateReq) {
        User updateUser = new User();
        updateUser.setId(userUpdateReq.getId());
        updateUser.setName(userUpdateReq.getName());
        updateUser.setAge(userUpdateReq.getAge());
        updateUser.setSex(userUpdateReq.getSex());
        return userManager.updateById(updateUser);
    }

    @Override
    public boolean deleteUser(Long id) {
        return userManager.deleteUser(id);
    }

}
