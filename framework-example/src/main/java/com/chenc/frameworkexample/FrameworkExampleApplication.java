package com.chenc.frameworkexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrameworkExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkExampleApplication.class, args);
    }

}
