package com.chenc.frameworkexample.util;

import com.chenc.common.util.DateTimeUtil;
import com.chenc.common.util.MathUtil;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.Date;

/**
 * 用一句话描述这个文件的作用
 *
 * @author Chen
 * @date 2022-03-19
 */
public class TestUtil {

    @Test
    public void testMath() {
        System.out.println(MathUtil.retainTwoPlaces(2.454));
        System.out.println(MathUtil.retainTwoPlaces(2.545));
        System.out.println(MathUtil.retainTwoPlaces(2.555));
        System.out.println(MathUtil.retainTwoPlaces(2.565));
    }

    @Test
    public void testDate() throws ParseException {
        Date date = new Date();
        String format = DateTimeUtil.format(date);
        System.out.println(format);
        System.out.println(DateTimeUtil.parse(format));
    }

}
